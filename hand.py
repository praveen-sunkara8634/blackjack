from variables import values

class Hand():

    def __init__(self):
        self.cards = [] # start with an empty list
        self.value = 0  # start with zero value
        self.aces = 0   # attribute to keep track of aces

    def add_card(self, card):
        # card passed in
        # from Deck.deal()
        self.cards.append(card)
        self.value += values[card.rank]

        # track aces
        if card.rank == 'Ace':
            self.aces += 1

    def adjust_for_ace(self):
        # if total value > 21 and I still have an ace
        # then change the value of my ace to be 1 instead of 11
        while self.value > 21 and self.aces:
            self.value -= 10
            self.aces -= 1