from card import Card
from deck import Deck
from hand import Hand
#Deck
test_deck = Deck()
test_deck.shuffle()

#Player
test_player = Hand()

#Deal one card from the deck
pulled_card = test_deck.deal()
print(pulled_card)
test_player.add_card(pulled_card)
print(test_player.value)