from deck import Deck
from hand import Hand
from chips import Chips


def player_busts(player, dealer, chips):
    print("BUST PLAYER!")
    chips.lose_bet()

def player_wins(player, dealer, chips):
    print("PLAYER WINS")
    chips.lose_bet()

def dealer_busts(player, dealer, chips):
    print('PLAYER WINS! DEALER BUSTED!')
    chips.win_bet()

def dealer_wins(player, dealer, chips):
    print("DEALER WINS!")
    chips.lose_bet()

def push(player, dealer):
    print("Dealer and Player tie! PUSH")

def show_some(player, dealer):

    print("DEALERS HAND: ")
    print("one card hidden!")
    print(dealer.cards[1])
    print("\n")
    print("PLAYERS HAND: ")
    for card in player.cards:
        print(card)

def show_all(player, dealer):

    print("DEALERS HAND: ")
    for card in dealer.cards:
        print(card)
    print("\n")
    print("PLAYERS HAND: ")
    for card in player.cards:
        print(card)

def take_bet(chips):
    while True:
        try:
            chips.bet = int(input("How many chips would you like to bet? "))
        except:
            print("Sorry please provide an integer")
        else:
            if chips.bet > chips.total:
                print(f"Sorry, you do not have enough chips! You have: {chips.total}")
            else:
                break

def hit(deck, hand):
    single_card = deck.deal()
    hand.add_card(single_card)
    hand.adjust_for_ace()


def hit_or_stand(deck, hand):
    global playing

    while True:
        x = input("Hit or Stand? Enter h or s ")

        if x[0].lower() == "h":
            hit(deck, hand)
        elif x[0].lower() == "s":
            print("Player Stands, Dealer's Turn")
            playing = False
        else:
            print("Sorry, I did not understand that, Please enter h or s only!")
            continue

        break

while True:
    # print an opening statement

    print("WELCOME TO BLACKJACK")
    #create and shuffle the deck, deal two cards to each player
    deck = Deck()
    deck.shuffle()

    player_hand = Hand()
    player_hand.add_card(deck.deal())
    player_hand.add_card(deck.deal())

    dealer_hand = Hand()
    dealer_hand.add_card(deck.deal())
    dealer_hand.add_card(deck.deal())

    # set up the Player's chips
    player_chips = Chips()

    # prompt the player for their bet
    take_bet(player_chips)

    # show cards (but keep one dealer card hidden)
    show_some(player_hand, dealer_hand)

    while playing:
        # prompt for player to Hit or Stand
        hit_or_stand(deck, player_hand)

        # show cards (but keep one dealer card hidden)
        show_some(player_hand, dealer_hand)

        # if player's hand exceeds 21, run player_busts() and break out of the loop
        if player_hand.value > 21:
            player_busts(player_hand, dealer_hand, player_chips)

            break

        # if player hasn't busted, play dealer's hand
        if player_hand.value <=21:

            while dealer_hand.value < player_hand.value:
                hit(deck, dealer_hand)

            # show all cards
            show_all(player_hand, dealer_hand)

            # run different winning scenarios
            if dealer_hand.value > 21:
                dealer_busts(player_hand, dealer_hand, player_chips)
            elif dealer_hand.value > player_hand.value:
                dealer_wins(player_hand, dealer_hand, player_chips)
            elif dealer_hand.value < player_hand.value:
                player_wins(player_hand, dealer_hand, player_chips)
            else:
                push(player_hand, dealer_hand)

        # inform the player about their total chips
        print(f"\n Player total chips are at: {player_chips.total}")

        # ask to play again
        new_game = input("Would you like to play another hand? y/n")

        if new_game[0].lower() == "y":
            playing = True
            continue
        else:
            print("Thank you for playing!")
            break

